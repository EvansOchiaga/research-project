 \chapter{Parametric Approach to Species Abundance Distribution}
 
Parametric approach is one of the major approaches used in describing species abundance distribution. This approach is based on the use of some parametric measures of SAD. In statistics parametric measures are defined as parameters of mathematically described probability distribution that can be interpreted based on some assumptions of a given probability distribution that is applied to a given data \cite{Brian2011}. This chapter discusses some parametric measures that has been put in place as far as SAD is concerned. In order to understand parametric measures as far as study of species abundance distribution is concerned then lets consider a given ecological community of species having $N$ individuals belonging to $S$ species, in this community there are different species having different number of individuals that is each distinct species has $N_1, N_2 ,\ldots, N_s$ individuals. The following is the diagrammatic representation of the ecological community having three species.
\begin{figure}[h!]
\centering
\begin{tikzpicture}
%\draw [step=1.0,blue] (-8,-8) grid (8,8) (0,0);
\draw (0,0) ellipse (4cm and 2cm);
\draw (0,0) ellipse (2cm and 0.5cm) (0,0)node{$N_2$} (0,1)node{Species 2};
\draw (-2,0) ellipse (0.5cm and 1cm) (-2.25,0)node{$N_1$} (-2,1.2)node{Species 1};
\draw (2,0) ellipse (0.5cm and 1cm) (2.25,0)node{$N_3$} (2,1.2)node{Species $3$};
\end{tikzpicture}
\caption{Diagrammatic representation of ecological community having three species. This community can be generalize for all the species in the environment, that is  $S_1,S_2,\ldots,S_i$ species with $N_1,N_2,\ldots,N_i$ individuals.}\label{community2}
\end{figure}
\FloatBarrier
The main interest of ecologist in studying species is to understand species abundance distribution and this is only possible if a sample of species is collected from the parent population and their abundance distribution is studied after which we can generalise about the species abundance distribution of the entire community. Therefore this calls for random sampling of some species from community (\ref{community2}). This sampling is based on the assumption that species are randomly distributed in community (\ref{community2}). In addition when we randomly collect a sample from community (\ref{community2}) then the number of individuals that will represent $j^\text{th}$ species in the collection will follow possion distribution with parameter $\lambda_j$, where parameter $\lambda_j$ is the mean abundance of the j$^\text{th}$ species. It now follows that the probability of $j^\text{th}$ species being represented by $r$ individuals in the sample is defined as follows \cite{Pielou1977}.

\begin{align}
\text{Pr}(\text{j$^{\text{th}}$} \hspace*{0.2cm}\text{species is represented by r individuals})=e^{-\lambda _j}\frac{\lambda_j^{r}}{r!}\label{equation1}
\end{align}
The probability in (\ref{equation1}) is specific for $j^\text{th}$ species. Now suppose that we are interested in studying species abundance distribution for the whole community then the probability shown in (\ref{equation1}) will be modified. The modification of the probability is brought about by variation of densities from species to species. Besides those, the total number of species in the community is assumed to be $S^{*}$ therefore 
several values of $\lambda$ is considered as constituting a sample of size $S^{*}$ from some continuous distribution of $\lambda$ having a pdf of $f(\lambda)$. Then the modified probability of $j^{\text{th}}$ species represented by $r$ individuals in the sample is the expectation of the probability of $j^{\text{th}}$ species being represented by $r$ individual in the collected sample defined as follows \cite{Pielou1977}.
\begin{align}
P_r=\int_{0}^{\infty}\frac{\lambda^ re^{-\lambda}}{r!}f(\lambda) d\lambda,\hspace*{0.3cm}r=0,1,2\ldots \label{equation2}
\end{align}
The probability distribution presented in (\ref{equation2}) is the distribution of the species frequencies $n_0$,$n_1,n_2,\ldots$, where $n_r$ is the expected frequency of species represented by $r$ individual in the sample defined as follows \cite{Pielou1977}.
\begin{align}
n_r=S^{*}P_r\label{equation3}
\end{align}
Furthermore observed distribution of species abundance distribution is a truncated type of theoretical distribution since zero class is not represented in the collection, this is due to the fact that little information is known about them. In calculation if S is the observed number of species in the samples collected then the missing class species is calculated as shown below \cite{Pielou1977}.
\begin{align}
S^{*}-S=n_0\label{equation4}
\end{align}
The next section discusses some families of compound poission distributions that is used in describing species abundance distribution as discussed by \cite{Pielou1977}.

\section{The Logarithmic Series or Log series Distribution}
Logarithm distribution is one of the parametric approaches put in place in describing species abundance distribution defined as follows \cite{Brian2011}.
\begin{align}
P_r(X=x)=K\frac{C^{x}}{x},\hspace*{0.3cm}K=-1/\log(1-C)\label{equation5}
\end{align}
When we assume that several values of $\lambda$ for different species collected from community (\ref{community2}) follows Pearson Type III distribution (Gamma distribution), that is $f(\lambda)$ is defined using the equation below.
\begin{align}
f(\lambda)=\frac{P^{-k}\lambda^{k-1}e^{-\frac{\lambda}{p}}}{\Gamma(k)},\hspace*{0.3cm}\lambda\geq 0\hspace*{0.2cm}\text{and}\hspace*{0.2cm} k,P>0\label{equation6}
\end{align}
Using (\ref{equation6}) and (\ref{equation2}), then probability that a species will be represented by $r$ individuals in the collection is as follows.
\begin{align}
P_r=\int_{0}^{\infty}\lambda^r\frac{e^{-\lambda}}{r!}\frac{P^{-k}\lambda^{k-1}e^{-\frac{\lambda}{p}}}{\Gamma(k)}d\lambda \label{equation7}
\end{align}
The solution of equation (\ref{equation7}) is a negative binomial distribution with probability $P_r$ defined as shown below.
\begin{align}
P_r=\frac{\Gamma(k+r)}{r!\Gamma(k)}\left(\frac{P}{1+P}\right)^r\left(\frac{1}{1+P}\right)^k,\hspace*{0.2cm}\text{where}\hspace*{0.2cm} r=0,1,2,\ldots \label{equation8}
\end{align}
Equation (\ref{equation8}) can further be simplified by letting $P/(1+P)=X$, we now have the following equation \cite{Pielou1977}.
\begin{align}
P_r=\frac{\Gamma(k+r)}{r!\Gamma(k)}(1-X)^kX^r,\hspace*{0.2cm}\text{where}\hspace*{0.2cm}0<X<1 \label{equation88}
\end{align}
Equation (\ref{equation88}) represents the probability that a given species will be represented by $r$ individuals in the collection without ignoring the zero class species. We now need to define a new probability $P_r^{'}$ which is the probability that a given species will be represented by $r$ individuals in the collection when zero class species is ignored. This probability distribution $P_r$ is a truncated negative binomials distribution and is calculated as shown below.
\begin{align}
P_r{'}=\frac{P_r}{1-P_0}=\frac{\Gamma(k+r)}{r!\Gamma(k)}\frac{X^r(1-X)^k}{[1-(1-X)^k]}, \hspace*{0.2cm}r=1,2,\ldots \label{equation8.1}
\end{align} 
where $P_0$ is the probability of the zero class species and calculated from (\ref{equation8}) as follows.
\begin{align}
P_0=\frac{\Gamma(k)}{0!\Gamma(k)}X^0(1-X)^k=(1-X)^k
\end{align}
In equation (\ref{equation8.1}) if we collect terms together that are independent of $r$ and substitute them by $C$ then we have the following.
\begin{align}
P_r^{'}=C\frac{\Gamma(k+r)}{r!}X^r,\hspace*{0.3cm}\text{and}\hspace*{0.2cm}C=\frac{(1-X)^k}{[1-(1-X)^k]}\frac{1}{\Gamma(k)}\label{equation8.2}
\end{align}
In equation (\ref{equation8.2}) parameter $k$ measures variability in the densities of different species. A large value of $k$ implies that the variability in the densities of species is very small and small value of $k$ implies that their is large variability in the densities of species. From a given defined ecological community it is clear that we have different species and this implies that their is high variation in the mean abundance of the species therefore it is very important to let $k \to 0$ in (\ref{equation8.2}). It follows that. 
\begin{align}
\lim_{k\to 0} P_r{'}=\lim_{k\to 0}C\frac{\Gamma(k+r)}{r!}X^r=\gamma\frac{X^{r}}{r},\hspace*{0.2cm}\text{where}\hspace*{0.2cm}\gamma=\lim_{k\to 0}C \label{equation8.3}
\end{align} 
Using equation (\ref{equation8.3}) then the expected frequency of species with $r$ individuals is as follows \cite{Pielou1977}. 
\begin{align}
n_r=SP_r{'}=S\gamma\frac{X^{r}}{r}=\alpha \frac{X^{r}}{r}\label{equation9}
\end{align} 
where $S$ is the total number of species observed in the collected sample from community (\ref{community2}). In addition using equation (\ref{equation9} ), the total number of species observed in the collection, $S$ and the total abundance of all the species in the collection, $N$ are defined as follows.
\begin{align}
S=\sum_{r=1}^\infty n_r=\sum_{r=1}^\infty\frac{\alpha X^r}{r}=-\alpha \log(1-X)
\end{align}
\begin{align}
N=\sum_{r=1}^{\infty}rn_r=\sum_{r=1}^{\infty}\frac{r\alpha X ^r}{r}=\frac{\alpha X}{1-X}
\end{align}
Equation (\ref{equation9}) is a log series distribution as shown in (\ref{equation5}) with parameters $\alpha$ and $X$. When the expected frequency in (\ref{equation9}) is plotted then we end up with a plot describing a clear picture of SAD as shown below.
\begin{figure}[h!]
	\centering
		\includegraphics[scale=0.5]{logseries.png}
	\caption{Log series distribution illustrating species abundance distribution with $X=0.2$ and $\alpha =2$}
\end{figure}\label{log-series}
\FloatBarrier

From figure (\ref{log-series}) we observe that it is right skewed. In relation to ecological community this plot implies that rare species are more abundant as compared to common species. This is due to the decreasing nature of the plot with increase in $r$ values. Using log series distribution in describing species abundance distribution there is one main disadvantage encountered, that is in deriving expression for probability that $j^\text{th}$ species will be represented by $r$ individual in the collection we let $k \to 0$ in (\ref{equation8.3}) and this implies that there is another uncollected zero class $k=0$ that has not been catered for in the collection, therefore using this method it is very hard to figure out the actual number of species $S^{*}$ in the parent population of species. Furthermore fitting this distribution to species abundance data is presented in section 2.5 alongside other parametric approaches to SAD presented in this chapter.

\section{Lognormal distribution}

Lognormal distribution is another important parametric approach that is used in describing SAD. Lognormal distribution is defined as a distribution whose variate conforms to the normal laws of probability \cite{Brown1957}. Probability density function of lognormal distribution is defined as shown below.
\begin{align}
f(\lambda)=\frac{1}{\lambda \sigma \sqrt{2\pi}}\exp\left[-\frac{1}{2 \sigma^2}\left(\ln\frac{\lambda}{m}\right)^2\right],\hspace*{0.2cm}\text{where}\hspace*{0.2cm} \lambda>0\label{equation10}
\end{align}
Where ln$\lambda$ follows normal distribution with mean ln $m$ and variance $\sigma^2$ \cite{Brown1957}. Suppose that several values of $\lambda$ of species in the sample collected from community (\ref{community2}) follows lognormal distribution, then using equation (\ref{equation10}), probability that a given species will be presented by $r$ individuals in the collection is as follows \cite{Pielou1977}.
\begin{align}
P_r=\int_{0}^{\infty} \lambda^r \frac{e^{-\lambda}}{r!}\frac{1}{\lambda \sigma^2 \sqrt{2\pi}}\exp\left[-\frac{1}{2 \sigma^2}\left(\ln\frac{\lambda}{m}\right)^2\right] d\lambda\label{equation11}
\end{align} 
\begin{align}
P_r=\frac{1}{r!\sigma\sqrt{2\pi}}\int_{0}^{\infty}\exp\left[-\lambda + r \ln \lambda-\frac{1}{2\sigma^2}(\ln \lambda-\ln m)^2\right]\frac{d \lambda}{\lambda}\label{equation11}
\end{align}
According to \cite{Pielou1969} equation (\ref{equation11}) can further be simplified by letting $\ln \lambda=x$ to give us below equation.
\begin{align}
P_r=\frac{1}{r!\sigma \sqrt{2\pi}}\int_{0}^{\infty}\exp \left[e^{-x}+rx-\frac{1}{2\sigma^2}(x-\ln m)^2\right]dx \label{equation11.1}
\end{align}
The probability distribution in (\ref{equation11.1}) is a poission lognormal distribution. This distribution is dependent on two parameters, medium species abundance $m$ and  variation of species abundance $\sigma^2$. In addition based on \cite{Pielou1977}, this probability distribution has no explicit expression for the integral. This distribution was first tested by Preston by using theoretical lognormal distribution. According to Preston he assumes that a given species is represented by its expected number of individuals in the collection and this is not affected by variation caused during sampling. In addition Preston grouped the values of $r$ into some groups called octaves such that the midpoint of each group is double that of proceeding one as shown below \cite{Pielou1977}.
\begin{align}
r=1,2,4,8,16,32,\ldots \label{equation12}
\end{align}
where $r$ is the grouping or octaves and the midpoint of the octaves are as follows.
\begin{align}
r=1\frac{1}{2},3,6,12,\ldots \label{equation13}
\end{align}
The groupings of $r$ implies that if a species falls on the boundary of the group, for example $2^{x}$ individuals, then it is considered to contribute half a species to the octave $(2^{x-1}\hspace*{0.1cm} \text{to} \hspace*{0.1cm}2^{x})$ and a half a species to $(2^{x} \hspace*{0.1cm}\text{to}\hspace*{0.1cm} 2^{x+1})$ as explained by \cite{Pielou1977}. This is similar to transforming species abundance with respect to $\log_2$ and the plot of the transformed abundance is as follows.
\begin{figure}[h!]
  \centering
      \includegraphics[width=0.5\textwidth]{octave.pdf}
  \caption{Histogram plot for BCI dataset for trees in the forest illustrating Preston octaves by transforming species abundance using $\log_2$.}\label{plot1}
\end{figure}
\FloatBarrier
In order to bring a clear picture of species abundance distribution as described by lognormal distribution, then it is very important to do a plot for lognormal distribution function and by using Sage the following plots were obtained.
\begin{figure}[h!]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{lognormal.png}
                \caption{Lognormal function plot}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{truncated.png}
                \caption{Truncated log normal function plot}
        \end{subfigure}
        \caption{Illustration of species abundance distribution based of Log normal distribution with mean $\mu=3$ and variance $\sigma^2=3$.}\label{plot2}
\end{figure}
\FloatBarrier

\subsection{Interpretation of the plots}

In figure (\ref{plot2}), the two plots for lognormal distribution that is truncated and non-truncated lognormal distribution, we observe that for the non-truncated one the curve rises to some maximum modal value and then starts decreasing, this implies that more abundance species will be some where to the right of the veil line which shouldn't be the case in real ecological community, on the other hand for truncated one that is, when we let the curve starts at some value of $r=1$, since we assume that no species can be represented by less than one individual in the community, it is clear that the plot of truncated lognormal distribution is a decreasing function from $r=1$ and this gives a clear picture of how species are distributed in our ecosystem. In addition plot illustrating octaves in (\ref{plot1}) gives a clear picture of lognormal shape of species abundance distribution.
In general when lognormal distribution is fitted to species abundance data and the plot for the histogram of SAD and the density curve are done on the same axis we end up with the following plot.
\begin{figure}[h!]
	\centering
		\includegraphics[scale=0.5]{log.pdf}
	\caption{Lognormal distribution fitted to \cite{BCI} dataset for the year 1982.}\label{log}
\end{figure}
\FloatBarrier
From plot (\ref{log}) it is clear that lognormal distribution gives a good fit to SAD data, but we can see that the curve rises to some modal value is when it starts decreasing and this implies that the most abundance species is somewhere to the right of the veil line which should not be the case due to the fact that in real ecological community rare species are  more as compared to common species and this is similar to interpretation for plots (\ref{plot2})a. However unlike log series distribution, when species abundance distribution is fitted using lognormal curve, it is possible to estimate the total number of species in the population including both collected and uncollected species \cite{Pielou1977}.

\section{Negative binomial distribution}

Negative binomial distribution is another fundamental distribution used by ecologist in describing species abundance. Negative binomial distribution is defined as follows \cite{Cathy2011} .
\begin{align}
f(x,y)=\frac{\Gamma(x+y)}{\Gamma(x)y!}p^{x}q^{y},\hspace*{0.2cm}\text{where}\hspace*{0.2cm}0<x<\infty,\hspace*{0.2cm}0<p<1\hspace*{0.2cm}\text{and}\hspace*{0.2cm}q=1-p \label{equation14}
\end{align}
From the previous section, it is clear that if ln $\lambda$ follows lognormal distribution then logarithmic plot of species abundance distribution will first rise to some modal values before starts decreasing, this is only possible if the sample is large enough for the veil line to fall to the left of the modal value. This implies that $\lambda$ has some modal value which is greater than zero and this is a clear indication that rare and common species are few as compared to some species having some intermediate value of $\lambda$. If we now assume that densities of several species follows Pearson Type III distribution then we have the following pdf for $\lambda$ \cite{Pielou1977}.
\begin{align}
f(\lambda)=\frac{1}{\Gamma(k)}P^{-k}\lambda^{k-1}e^{\frac{-\lambda}{P}} \label{equation15}
\end{align}
Equation (\ref{equation15}) implies that the expected frequency of species with $r$ individuals without letting $k\to 0$ will follow negative binomial distribution.
We are interested in analysing whether $f(\lambda)$ is a monotonically decreasing function at some value of $\lambda=0$ or if it has some modal value at $\lambda>0$. To check for this property then in equation (\ref{equation15}) we let $P$ to be a constant and we differentiate (\ref{equation15}) with respect to $\lambda$. In doing so we have the following equation \cite{Pielou1977}.
\begin{align}
\frac{df(\lambda)}{d\lambda}=\frac{1}{\Gamma(k)}P^{-k}(k-1)\lambda^{k-2}-\frac{\lambda}{P}e^{\frac{-\lambda}{P}}\label{equation16}
\end{align}
\begin{align}
\frac{df(\lambda)}{d\lambda}=\frac{1}{\Gamma(k)}P^{-k}\lambda^{k-2}e^{\frac{-\lambda}{P}}\left\lbrace k-1-\frac{\lambda}{P}\right\rbrace \label{equation17}
\end{align}
In (\ref{equation17}), if $k>1$ then $f(\lambda)$ will has its maximum if $\lambda=P(k-1)$ .
Therefore this implies that if the species abundance distribution is fitted using truncated negative binomial distribution with $k>1$, then some intermediate species will be more as compared to rare and common species. In addition if $0\leq k\leq 1$, then $\frac{df(\lambda)}{d\lambda}$ is negative for all values of $\lambda$ and this implies $f(\lambda)$ will have its maximum in the range of $0\leq k\leq 1$, hence rare species will be more as compared to abundant species \cite{Pielou1977}. The two conditions for negative binomial distribution that describes species abundance is summarized in the plots below.
\begin{figure}[h!]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{k>1.png}
                \caption{Negative binomial distribution with $k>1$}
        \end{subfigure}\label{plot3a}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{negativebinomial.png}
                \caption{Negative binomial distribution with $0\leq k \leq 1$}
        \end{subfigure}\label{plot3b}
        \caption{Two plots of negative binomial distribution conditions showing description of species abundance with $p=40$ and values of $k$ are $k>1$ and $0\leq k\leq1$ for (a) and (b) respectively.}\label{plot3}
\end{figure}
\FloatBarrier
Figure (\ref{plot3}) gives a clear picture when negative binomial distribution is used in describing species abundance distribution using different conditions of $k$ as explained in equation (\ref{equation17}). From (\ref{plot3}a) it is clear that some intermediate species will be more as compared to rare and common species also from (\ref{plot3}b) we can observe that rare species will be more abundant as compared to common species and this is the same interpretation given using equation (\ref{equation17}).

\section{Geometric distribution}
Geometric distribution is a special case of negative binomial distribution defined as follows \cite{Dennis2008}.
\begin{align}
f(y)=q^{y-1}p,\hspace*{0.2cm}\text{where}\hspace*{0.2cm}y=1,2\ldots\hspace*{0.2cm}\text{and}\hspace*{0.2cm}0\leq p \leq 1 \label{equation18}
\end{align}
From the previous section it is clear that when several values of $\lambda$ follows Pearson Type III distribution and we let $k\to 0$ this leads to log series distribution of species abundance, in addition if we let $k$ to be defined by the data then this yields a negative binomial distribution explaining species abundance distribution. If we now let $k=1$ in (\ref{equation15}), this will yield for us the following probability density function  for $\lambda$ \cite{Pielou1977}.
\begin{align}
f(\lambda)=\frac{1}{\Gamma(1)}P^{-1}\lambda^0e^{\frac{-\lambda}{P}}\label{equation19}
\end{align}
\begin{align}
f(\lambda)=\frac{1}{P}e^{\frac{-\lambda}{P}},\hspace*{0.2cm}\text{where}\hspace*{0.2cm}\lambda\geq 0\label{equation20}
\end{align}
Equation (\ref{equation20}) is probability density function of exponential distribution. In addition when we put $k=1$ in (\ref{equation8}), this will yield for us geometric distribution which is a discrete case of exponential distribution in (\ref{equation20}). We now have the following equation \cite{Pielou1977}. 
\begin{align}
P_r^{'}=\frac{\Gamma(1+r)}{r!\Gamma(1)}\frac{P^r}{(1+P)[1-(1+P)^{-1}]}\label{equation21}
\end{align}
\begin{align}
P_r^{'}=\left(\frac{P}{1+P}\right)^{r-1}\left(\frac{1}{1+P}\right),\hspace*{0.2cm}\text{where}\hspace*{0.2cm}
r=1,2\ldots \label{equation22}
\end{align}
When we now plot geometric distribution in (\ref{equation22}) that describes species abundance distribution as a function of some $r$ values, then we end up with the following plot that is used in describing species abundance distribution in the community.
\begin{figure}[h!]
	\centering
		\includegraphics[scale=0.5]{geometric.png}
	\caption{Illustration of species abundance distribution using geometric distribution with $p=0.3$}\label{geometric}
\end{figure}
\FloatBarrier
From the figure (\ref{geometric}) we observe that it is a decreasing function with increase in the value of $r$. In relation to species abundance distribution, this plot is interpreted as rare species are more as compared to common species, which is the case for any ecological community. Even though this distribution has been tested in many different ways apart from the one presented in this section, it still emerge the best as compared to negative binomial and log series distribution as we shall see in the next section.
\section{Evaluating parametric approach}
In evaluating which distribution best fit the species abundance data, rank species abundance was plotted together with rank abundance distributions. Rank species abundance is used in this section to mean that most abundant species i.e. single tone species is represented by rank one, then the second abundance species is represented by rank two and the system continues for the rest of the species in the community. The following plots were obtained.
\begin{figure}[h!]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{rank.pdf}
                \caption{Rank Species Abundance.}
        \end{subfigure}\label{plot2a}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{Rank.pdf}
                \caption{Fitting Distributions to Species Abundance data.}
        \end{subfigure}\label{plot3b}
        \caption{A few common rank abundance distributions fitted using \cite{BCI} data for the year 1982. The log normal curve best fits the data.}\label{evaluation}
\end{figure}
\FloatBarrier
From figure (\ref{evaluation}b) it is clear that log normal distribution is the best in fitting and describing species abundance distribution since the log normal density curve matches with the rank species abundance curve. In addition we can observe that all the plots takes the shape of decreasing function and this implies that rare species are more as compared to common species and this the same interpretation presented in the previous sections of this chapter. The following chapter present some mechanistic approaches that are also used in describing species abundance distribution alongside the parametric approaches. 
